package com.company;

import java.awt.*;

public interface Vehicle {
    void run(int fields);
    void back(int fields);
    Point getPosition();
    void rotate(String direction);
}
