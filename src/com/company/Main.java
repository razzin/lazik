package com.company;
// Autor: Adam Kulas, 32016
public class Main {

    public static void main(String[] args) {
        Lazik lazik= new Lazik();
        System.out.println("Pozycja startowa:" + lazik.getPosition());
        lazik.rotate("RIGHT");
        lazik.run(2);
        lazik.rotate("LEFT");
        lazik.back(2);
        lazik.rotate("RIGHT");
        lazik.back(2);
        lazik.rotate("LEFT");
        lazik.run(1);
        System.out.println("Cel osiągnięty.");
        System.out.println("Pozycja pojazdu: "+ lazik.getPosition().x + ", "+ lazik.getPosition().y + ", zwrot pojazdu: " + lazik.getTurn());
    }
}
