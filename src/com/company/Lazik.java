package com.company;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class Lazik implements Vehicle{
    private Point position;
    private char turn;

    Lazik(){
        position=new Point(0,0);
        setTurn('N');
    }

    @Override
    public void run(int fields){
        if (getTurn() == 'N') {
            position.y += fields;
        } else if (getTurn() == 'E') {
            position.x += fields;
        } else if (getTurn() == 'S') {
            position.y -= fields;
        } else if (getTurn() == 'W') {
            position.x -= fields;
        } else {
            System.out.println("Wystąpił błąd, prosimy skontaktować się z najbliższą bazą.");    //brak obsługi wyjątków
        }
    }

    @Override
    public void back(int fields) {
        run(-fields);
    }     // cofanie to przecież ruch do przodu z przeciwną wartością.

    @Override
    public Point getPosition() {
        return position;
    }

    @Override
    public void rotate(String direction) {
        List<Character> turns = Arrays.asList('N','E','S','W'); // tablica do zmiany kierunków łazika

        if(direction=="LEFT"){
            if(turns.indexOf(getTurn())==0){   // jeśli łazik jest skierowany na północ, skręt w lewo oznacza przejście do ostatniego (3) elementu tablicy - zachód
                setTurn(turns.get(3));
            }
            else setTurn(turns.get(turns.indexOf(getTurn()) - 1));
        }
        else if(direction=="RIGHT"){
            if(turns.indexOf(getTurn())==3){   // jeśli łazik jest skierowany na zachód, skręt w prawo oznacza przejście do pierwszego (0) elementu tablicy - północ
                setTurn(turns.get(0));
            }
            else setTurn(turns.get(turns.indexOf(getTurn()) + 1));
        }
        else{
            System.out.println("ROTATE ERROR");  //brak obsługi wyjątków
        }
    }

    public char getTurn() {
        return turn;
    }

    public void setTurn(char turn) {
        this.turn = turn;
    }
}
